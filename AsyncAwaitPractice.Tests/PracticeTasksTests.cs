﻿using AsyncAwaitPractice.Domain;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AsyncAwaitPractice.Tests
{
    [TestFixture]
    public class PracticeTasksTests
    {
        private PracticeTasks practiceTasks;

        [SetUp]
        public void SetUp()
        {
            practiceTasks = new PracticeTasks();
        }

        [Test]
        public async Task CreateArrayOfTenRandomIntegers_ReturnsArrayWithLengthEqualToTen()
        {
            var expectedLength = 10;

            var arrayOfTenRandomIntegers = await practiceTasks.CreateArrayOfTenRandomIntegers();
            var actualLength = arrayOfTenRandomIntegers.Length;

            Assert.AreEqual(expectedLength, actualLength);
        }

        [Test]
        public async Task CreateArrayOfTenRandomIntegers_ProperlyPopulatedByRandomNumbers()
        {
            var randomIntegersArray = await practiceTasks.CreateArrayOfTenRandomIntegers();
            bool hasDiverseValues = randomIntegersArray.Any(number => number != default);

            Assert.IsTrue(hasDiverseValues, "Values weren't populated properly or test was really unlucky");
        }

        [Test]
        public void MultiplyArrayElementsByRandomValue_ArrayIsNull_ThrowsArgumentNullException()
        {
            var ex = Assert.ThrowsAsync<ArgumentNullException>(() => practiceTasks.MultiplyArrayElementsByRandomValue(null));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArrayWithOutOfRangeElements))]
        public void MultiplyArrayElementsByRandomValue_AtleastOneArrayElementHasInvalidValue_ThrowsArgumentOutOfRangeException(int[] array)
        {
            var ex = Assert.ThrowsAsync<ArgumentOutOfRangeException>(() => practiceTasks.MultiplyArrayElementsByRandomValue(array));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArraysWithValidInput))]
        public async Task MultiplyArrayElementsByRandomValue_ValidInput_CorrectlyMultipliesValues(int[] originalArray)
        {
            var (actualArray, multiplier) = await practiceTasks.MultiplyArrayElementsByRandomValue(originalArray);
            var expectedArray = originalArray.Select(number => number * multiplier).ToArray();

            Assert.AreEqual(expectedArray, actualArray);
        }

        [Test]
        public void SortArrayByAscending_ArrayIsNull_ThrowsArgumentNullException()
        {
            var ex = Assert.ThrowsAsync<ArgumentNullException>(() => practiceTasks.SortArrayByAscending(null));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArraysWithValidInput))]
        public async Task SortArrayByAscending_ValidInput_ReturnsSortedArray(int[] array)
        {
            var expected = array;
            Array.Sort(expected);

            var actual = await practiceTasks.SortArrayByAscending(array);

            Assert.AreEqual(expected, actual);
        }

        [Test]
        public void GetAverageValue_ArrayIsNull_ThrowsArgumentNullException()
        {
            var ex = Assert.ThrowsAsync<ArgumentNullException>(() => practiceTasks.GetAverageValue(null));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [Test]
        public void GetAverageValue_ArrayIsEmpty_ThrowsArgumentException()
        {
            var array = new int[0];
            var ex = Assert.ThrowsAsync<ArgumentException>(() => practiceTasks.GetAverageValue(array));
            Assert.That(ex.ParamName, Is.EqualTo("array"));
        }

        [TestCaseSource(nameof(ArraysWithValidInput))]
        public async Task GetAverageValue_ValidInput_CorrectlyReturnsAverageValue(int[] array)
        {
            var expected = array.Average();

            var actual = await practiceTasks.GetAverageValue(array);

            Assert.AreEqual(expected, actual);
        }


        private static IEnumerable<int[]> ArrayWithOutOfRangeElements()
        {
            yield return new int[] { PracticeTasks.MinRandomValue - 1 };
            yield return new int[] { PracticeTasks.MaxRandomValue + 1 };
        }

        private static IEnumerable<int[]> ArraysWithValidInput()
        {
            yield return new int[] { 100_000 };
            yield return new int[] { 0, 1, 2, 3, 4, 5 };
            yield return new int[] { PracticeTasks.MinRandomValue, 50, PracticeTasks.MaxRandomValue, 0 };
        }
    }
}
