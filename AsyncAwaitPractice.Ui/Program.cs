﻿using AsyncAwaitPractice.Domain;
using System;
using System.Threading.Tasks;

namespace AsyncAwaitPractice.Ui
{
    internal static class Program
    {
        static async Task Main()
        {
            var practiceTasks = new PracticeTasks();

            var arrayOfRandomIntegers = await practiceTasks.CreateArrayOfTenRandomIntegers();
            var (multipliedArray, _) = await practiceTasks.MultiplyArrayElementsByRandomValue(arrayOfRandomIntegers);
            var sortedAndMultipliedArray = await practiceTasks.SortArrayByAscending(multipliedArray);
            var average = await practiceTasks.GetAverageValue(sortedAndMultipliedArray);

            Console.WriteLine($"\n---\nAverage after finishing all tasks = {average}");
            Console.ReadLine();
        }
    }
}
