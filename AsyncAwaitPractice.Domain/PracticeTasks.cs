﻿using System;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AsyncAwaitPractice.Domain
{
    public class PracticeTasks
    {
        private readonly Random random = new Random();
        public const int MaxRandomValue = 100_000;
        public const int MinRandomValue = -100_000;

        public async Task<int[]> CreateArrayOfTenRandomIntegers()
        {
            return await Task.Run(() =>
            {
                var arrayToReturn = new int[10];
                for (int i = 0; i < arrayToReturn.Length; i++)
                {
                    arrayToReturn[i] = random.Next(MinRandomValue, MaxRandomValue);
                }

                Console.WriteLine($"Created Array of 10 random integers with values:");
                PrintArrayValues(arrayToReturn);

                return arrayToReturn;
            });
        }

        public async Task<(int[] MultipliedArray, int Multiplier)> MultiplyArrayElementsByRandomValue(int[] array)
        {
            return await Task.Run(() =>
            {
                ValidateArrayNotNull(array);
                ValidateArrayElementsInValidRanges(array);

                var multiplier = random.Next(MinRandomValue, MaxRandomValue);
                var arrayToReturn = new int[array.Length];

                for (int i = 0; i < arrayToReturn.Length; i++)
                {
                    arrayToReturn[i] = array[i] * multiplier;
                }

                Console.WriteLine($"Array values after multiplication by {multiplier}:");
                PrintArrayValues(arrayToReturn);

                return (arrayToReturn, multiplier);
            });
        }

        public async Task<int[]> SortArrayByAscending(int[] array)
        {
            return await Task.Run(() =>
            {
                ValidateArrayNotNull(array);

                Array.Sort(array);

                Console.WriteLine("Array values after sorting by ascending:");
                PrintArrayValues(array);

                return array;
            });
        }

        public async Task<double> GetAverageValue(int[] array)
        {
            return await Task.Run(() =>
            {
                ValidateArrayNotNull(array);
                ValidateArrayNotEmpty(array);

                var averageValue = array.Average();

                Console.WriteLine($"Array's average value = {averageValue}");

                return averageValue;
            });
        }

        private void ValidateArrayNotNull(int[] array)
        {
            if (array is null)
            {
                throw new ArgumentNullException(nameof(array));
            }
        }

        private void ValidateArrayElementsInValidRanges(int[] array)
        {
            if (array.Any(number => number < MinRandomValue || number > MaxRandomValue))
            {
                var outOfRangeErrorMessage = $"At least one of the array elements is outside the range of allowed values. Allowed range is inbetween {MinRandomValue} and {MaxRandomValue}";
                throw new ArgumentOutOfRangeException(nameof(array), outOfRangeErrorMessage);
            }
        }

        private void ValidateArrayNotEmpty(int[] array)
        {
            if (array.Length == 0)
            {
                throw new ArgumentException("It is not possible to calculate the average from the empty array.", nameof(array));
            }
        }

        private void PrintArrayValues(int[] array)
        {
            var stringBuilder = new StringBuilder();
            for (int i = 0; i < array.Length; i++)
            {
                stringBuilder.AppendLine($"Index {i} Value = {array[i]}");
            }
            Console.WriteLine(stringBuilder.ToString());
        }
    }
}
